<?php

// for netbeans to deal with $app var as slim

if (false) {
    $app = new \Slim\Slim();
}
 $globalCategoriesList = DB::query("SELECT * FROM categories");
// populate select status enum values
$globalStatusesList = array("New", "Good condition Brand new", "In used Brand new");

// populate select size enum values
$globalSizesList = array("Small", "Medium", "Large", "X-Large");

// STATE 1: first show
// in case of get method request
$app->get('/admin/items/:action(/:id)', function($action, $id = 0) use ($app) {
    global $globalStatusesList, $globalSizesList, $globalCategoriesList;

    if (!isset($_SESSION['user']) || $_SESSION['user']['role'] != 'admin') {
        $app->redirect('/forbidden');
        return;
    }
    // invalid process, id==0 true means id is not provided
    if (($action == 'add' && $id != 0) || ($action == 'edit' && $id == 0)) {

        $app->notFound(); // 404 page 
        return;
    }
    // adding item
    if ($action == 'add') {
        // populate select category from database
        global $globalCategoriesList;
       
        if (!$globalCategoriesList) {

            $app->notFound();
            return;
        }




        $app->render('admin/items_addedit.html.twig', array(
            'categoriesList' => $globalCategoriesList,
            'statusList' => $globalStatusesList,
            'sizeList' => $globalSizesList));
    } else { // edit
        // selected for edit
        $item = DB::queryFirstRow("SELECT * FROM items WHERE id=%i", $id);
        if (!$item) {
            //    $log->debug("aaa 1");
            $app->notFound();
            return;
        }
      
        global $globalCategoriesList;
        $app->render('admin/items_addedit.html.twig', array('v' => $item,
            'categoriesList' => $globalCategoriesList,
            'statusList' => $globalStatusesList,
            'sizeList' => $globalSizesList));
    }
})->conditions(array('action' => '(add|edit)'));
// in case of post method request
$app->post('/admin/items/:action(/:id)', function($action, $id = 0) use ($app) {
    if (!isset($_SESSION['user']) || $_SESSION['user']['role'] != 'admin') {
        $app->redirect('/forbidden');
        return;
    }
    if (($action == 'add' && $id != 0) || ($action == 'edit' && $id == 0)) {
        $app->notFound(); // 404 page
        return;
    }
    // print_r($_POST); // TO CHECK post data
    // Are we receiving submission
    // array to store errors list
    $errorList = array();
    $name = $app->request()->post('name');
    $description = $app->request()->post('description');
    $categoryId = $app->request()->post('category');
    $brand = $app->request()->post('brand');
    $status = $app->request()->post('status');
    $color = $app->request()->post('color');
    $size = $app->request()->post('size');
    $price = $app->request()->post('price');
    $qty = $app->request()->post('unitsInStock');
    $color = $app->request()->post('color');
    // upload image from file
    $productImage = $_FILES['image'];
    // validation for image
    if ($productImage['error'] != 0) {
        array_push($errorList, "File submission failed, make sure you've selected an image (1)");
    } else {
        $data = getimagesize($productImage['tmp_name']);
        if ($data == FALSE) {
            array_push($errorList, "File submission failed, make sure you've selected an image (2)");
        } else {
            if (!in_array($data['mime'], array('image/jpeg', 'image/gif', 'image/png'))) {
                array_push($errorList, "File submission failed, make sure you've selected an image (3)");
            } else {
                // FIXME: sanitize file name, otherwise a security hole, maybe
                $productImage['name'] = strtolower($productImage['name']);
                if (!preg_match('/.\.(jpg|jpeg|png|gif)$/', $productImage['name'])) {
                    array_push($errorList, "File submission failed, make sure you've selected an image (4)");
                }
                $productImage['name'] = preg_replace('[^a-zA-Z0-9_\.-]', '_', $productImage['name']);
                if (file_exists('uploads/' . $productImage['name'])) {
                    // array_push($errorList, "File submission failed, refusing to override existing file (5)");
                    $num = 1;
                    $info = pathinfo($productImage['name']);
                    while (file_exists('uploads/' . $info['filename'] . "_$num." . $info['extension'])) {
                        $num++;
                    }
                    $productImage['name'] = $info['filename'] . "_$num." . $info['extension'];
                }
                // all good, nothing to do for now
            }
        }
    }

    // FIXME: sanitize html tags in name and description
    // validation for item name
    if (strlen($name) < 2 || strlen($name) > 50) {
        array_push($errorList, "Name must be 2-100 characters long");
        $name = "";
    }
    // validation for item description
    if (strlen($description) < 2 || strlen($description) > 200) {
        array_push($errorList, "Description must be 2-200 characters long");
        $description = "";
    }
    // validation for item category
    if (!isset($categoryId)) {
        array_push($errorList, "Please select category");
    }

    // validation for item price
    if ($price == "" || $price < 0 || $price > 999999.99) {
        array_push($errorList, "Price empty or out of range");
        $price = "";
    }
    if ($errorList) { // STATE 2: failed submission
        global $globalStatusesList, $globalSizesList, $globalCategoriesList;
        $app->render('admin/items_addedit.html.twig', array(
            'errorList' => $errorList,
            //populate v to keep correct values
            'v' => array('id' => $id,
                'name' => $name,
                'description' => $description,
                'brand' => $brand,
                'price' => $price,
                'unitsInStock' => $qty),
            'categoriesList' => $globalCategoriesList,
            'statusList' => $globalStatusesList,
            'sizeList' => $globalSizesList));
    } else { // STATE 3: successful submission
        // get the image path
        $imagePath = 'uploads/' . $productImage['name'];
        // DANGERS: // uploads/../slimshop17.php
        // 1. what if name begins with .. and escapes to an upper directory?
        // 2. what if the file extension is dangerous, e.g. php
        // 3. file overriding
        // check if file path move to correct location
        if (!move_uploaded_file($productImage['tmp_name'], $imagePath)) {
            $log->err("Error moving uploaded file: " . print_r($productImage, true));
            $app->redirect('/internalerror');
            return;
        }
        if ($action == 'add') {
            DB::insert('items', array('name' => $name,
                'description' => $description,
                'brand' => $brand,
                'price' => $price,
                'unitsInStock' => $qty,
                'categoryId' => $categoryId,
                'status' => $status,
                'size' => $size,
                'color' => $color,
                'imagePath' => $imagePath));
            $app->render('admin/items_addedit_success.html.twig');
        } else {// edit
            // remove the old file
            $oldImagePath = DB::queryFirstField("SELECT imagePath FROM items WHERE id=%i", $id);
            if ($oldImagePath != "" && file_exists($oldImagePath)) {
                unlink($oldImagePath);
            }

            DB::update('items', array('name' => $name,
                'description' => $description,
                'brand' => $brand,
                'price' => $price,
                'unitsInStock' => $qty,
                'categoryId' => $categoryId,
                'status' => $status,
                'size' => $size,
                'color' => $color,
                'imagePath' => $imagePath), 'id=%i', $id);
            $app->render('admin/items_addedit_success.html.twig', array('savedId' => $id));
        }
    }
})->conditions(array('action' => '(add|edit)'));


// delete action
$app->get('/admin/items/delete/:id', function($id) use ($app, $log) {
    if (!isset($_SESSION['user']) || $_SESSION['user']['role'] != 'admin') {
        $app->redirect('/forbidden');
        return;
    }//
    $item = DB::queryFirstRow("SELECT * FROM items WHERE id=%i", $id);
    if (!$item) {
        $app->notFound();
        return;
    }
    $app->render('admin/items_delete.html.twig', array('item' => $item));
});
$app->post('/admin/items/delete/:id', function($id) use ($app, $log) {
    if (!isset($_SESSION['user']) || $_SESSION['user']['role'] != 'admin') {
        $app->redirect('/forbidden');
        return;
    }//
    if ($app->request()->post('confirmed') == 'true') {
        DB::delete("items", "id=%i", $id);
        $app->render('admin/items_delete_success.html.twig');
    } else {
        $app->redirect('/internalerror');
        return;
    }
});

