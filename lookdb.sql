-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3333
-- Generation Time: Jul 03, 2019 at 11:27 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lookdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `cartitems`
--

CREATE TABLE `cartitems` (
  `ID` int(11) NOT NULL,
  `sessionID` varchar(50) NOT NULL,
  `itemID` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `createdTS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cartitems`
--

INSERT INTO `cartitems` (`ID`, `sessionID`, `itemID`, `quantity`, `createdTS`) VALUES
(1, 'l5oqedenhu90v0u0m4htar7e24', 4, 8, '2019-07-01 02:09:49'),
(6, 'l5oqedenhu90v0u0m4htar7e24', 5, 1, '2019-07-01 03:37:32'),
(7, 'l5oqedenhu90v0u0m4htar7e24', 7, 2, '2019-07-01 03:48:35'),
(10, 'im5c2jc8ugpb88ij26o1tdkda1', 6, 12, '2019-07-01 18:02:28'),
(11, 's26sgvbgvricnqr0p3qdf9peg7', 4, 1, '2019-07-01 22:28:41'),
(12, '5v5gp4jhdhsid5drp6vtunvjk3', 4, 1, '2019-07-02 22:49:15');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`) VALUES
(1, 'Clothing'),
(2, 'Shoes'),
(3, 'Beauty'),
(4, 'Handbags & Accessories'),
(5, 'Sport');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL,
  `categoryId` int(11) NOT NULL,
  `brand` varchar(50) NOT NULL,
  `status` enum('New','Good condition Brand new','In used Brand new') NOT NULL,
  `color` varchar(20) NOT NULL,
  `size` enum('Small','Medium','Larg','X-Large') NOT NULL,
  `imagePath` varchar(50) NOT NULL,
  `prvPrice` decimal(6,2) DEFAULT NULL,
  `price` decimal(6,2) NOT NULL,
  `unitsInStock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `name`, `description`, `categoryId`, `brand`, `status`, `color`, `size`, `imagePath`, `prvPrice`, `price`, `unitsInStock`) VALUES
(4, 'dress2', 'Women Clothing', 1, 'Gucci', 'Good condition Brand new', '#ffffff', 'Small', 'uploads/product-2.jpg', '75.00', '60.00', 3),
(5, 'Pyjama', 'Women Clothing', 1, 'Chanel', 'New', '#000000', 'Small', 'uploads/product-3.jpg', '30.00', '24.00', 10),
(6, 'blouse', 'Women Clothing', 1, 'Valentino', 'New', '#e3f50a', 'Medium', 'uploads/product-5.jpg', '63.00', '50.00', 4),
(7, 'winter dress', 'Women Clothing', 1, 'Gucci', 'In used Brand new', '#000000', 'Larg', 'uploads/product-6.jpg', '110.00', '90.00', 3),
(8, 'Chemise', 'Men Clothing', 1, 'Valentino', 'New', '#ff00ff', 'Medium', 'uploads/chemise.jpg', '82.00', '70.00', 4),
(9, 'mantaux', 'Men Clothing', 1, 'Gucci', 'Good condition Brand new', '#000000', 'X-Large', 'uploads/mantaux.jpg', '120.00', '100.00', 6),
(10, 'plaiser', 'Men Clothing', 1, 'Prada', 'New', '#0080c0', 'Small', 'uploads/plaiser_1.jpg', '109.00', '99.00', 7),
(16, 'Sweeter', 'Men Clothing', 1, 'Prada', 'Good condition Brand new', '#000000', '', 'uploads/sweeter_4.jpg', '130.00', '100.00', 5),
(17, 'kids3', 'baby clothing', 4, 'Valentino', 'Good condition Brand new', '#ffffff', 'Small', 'uploads/kid3.jpg', '33.00', '25.00', 3),
(19, 'kid1', 'Kids Clothing', 1, 'Chanel', 'New', '#faf1fa', 'Small', 'uploads/kid1_1.jpg', '89.00', '70.00', 3),
(20, 'kid2', 'Kids Clothing', 1, 'Valentino', 'In used Brand new', '#0000a0', 'Small', 'uploads/kid2.jpg', '75.00', '60.00', 10),
(21, 'kid4', 'Kids Clothing', 1, 'Prada', 'New', '#ffffff', 'Small', 'uploads/kid4.jpg', '30.00', '22.00', 3);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `subject` varchar(50) NOT NULL,
  `message` text NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `orderitems`
--

CREATE TABLE `orderitems` (
  `ID` int(11) NOT NULL,
  `orderID` int(11) NOT NULL,
  `origItemID` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orderitems`
--

INSERT INTO `orderitems` (`ID`, `orderID`, `origItemID`, `price`, `quantity`) VALUES
(1, 5, 8, '70.00', 1),
(2, 6, 6, '50.00', 2),
(3, 7, 4, '60.00', 1),
(4, 7, 5, '24.00', 2),
(5, 7, 6, '50.00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `ID` int(11) NOT NULL,
  `userID` int(11) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `address` varchar(100) NOT NULL,
  `postalCode` varchar(6) NOT NULL,
  `email` varchar(250) NOT NULL,
  `phoneNumber` varchar(10) NOT NULL,
  `totalBeforeTax` decimal(10,2) NOT NULL,
  `shippingBeforeTax` decimal(10,2) NOT NULL,
  `taxes` decimal(10,2) NOT NULL,
  `totalWithShippingAndTaxes` decimal(10,2) NOT NULL,
  `dateTimePlaced` datetime NOT NULL,
  `dateTimeShipped` datetime DEFAULT NULL,
  `status` enum('placed','shipped','cancelled','delivered') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`ID`, `userID`, `name`, `address`, `postalCode`, `email`, `phoneNumber`, `totalBeforeTax`, `shippingBeforeTax`, `taxes`, `totalWithShippingAndTaxes`, `dateTimePlaced`, `dateTimeShipped`, `status`) VALUES
(3, NULL, 'Jerry J', '1234 some st.', 'H8U1R3', 'jerry@jerry.com', '514-555-12', '107.73', '15.00', '18.41', '141.14', '2016-10-13 18:54:16', NULL, 'placed'),
(5, NULL, 'John Smith', '1240 boul. Guin Pierrefonds', 'h8k 1a', 'johnsmith@gmail.com', '514-654-85', '70.00', '15.00', '12.75', '97.75', '2019-07-01 19:39:01', NULL, 'placed'),
(6, NULL, 'Rena', '1240 boul. Chateaupierrefonds Pierrefonds', 'h9k 1a', 'rena@gmail.com', '514-998-85', '100.00', '15.00', '17.25', '132.25', '2019-07-01 19:50:04', NULL, 'placed'),
(7, NULL, 'John Smith', '1240 boul. Guin Pierrefonds', 'h8k 1a', 'johnsmith@gmail.com', '514-654-85', '158.00', '15.00', '25.95', '198.95', '2019-07-03 06:02:31', NULL, 'placed');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(20) NOT NULL,
  `role` enum('admin','customer','employee','seller') NOT NULL DEFAULT 'customer'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `password`, `role`) VALUES
(1, 'Yasmeen', 'Boules', 'yasmeenboules@gmail.com', 'Felix2017', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cartitems`
--
ALTER TABLE `cartitems`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `sessionID_2` (`sessionID`,`itemID`),
  ADD KEY `sessionID` (`sessionID`),
  ADD KEY `productID` (`itemID`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categoryId` (`categoryId`),
  ADD KEY `items_ibfk_1` (`imagePath`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`);

--
-- Indexes for table `orderitems`
--
ALTER TABLE `orderitems`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `origProductID` (`origItemID`),
  ADD KEY `orderID` (`orderID`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `userID` (`userID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cartitems`
--
ALTER TABLE `cartitems`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orderitems`
--
ALTER TABLE `orderitems`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cartitems`
--
ALTER TABLE `cartitems`
  ADD CONSTRAINT `cartitems_ibfk_1` FOREIGN KEY (`itemID`) REFERENCES `items` (`id`);

--
-- Constraints for table `items`
--
ALTER TABLE `items`
  ADD CONSTRAINT `items_ibfk_1` FOREIGN KEY (`categoryId`) REFERENCES `categories` (`id`);

--
-- Constraints for table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `messages_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`);

--
-- Constraints for table `orderitems`
--
ALTER TABLE `orderitems`
  ADD CONSTRAINT `orderitems_ibfk_1` FOREIGN KEY (`orderID`) REFERENCES `orders` (`ID`),
  ADD CONSTRAINT `orderitems_ibfk_2` FOREIGN KEY (`origItemID`) REFERENCES `items` (`id`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
