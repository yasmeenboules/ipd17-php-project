<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* login.html.twig */
class __TwigTemplate_95cc7a385968078320ce42ca371577f432c358a532a7ac4f1b550c564f6b453a extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'headAdd' => [$this, 'block_headAdd'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "login.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        echo "login";
    }

    // line 5
    public function block_headAdd($context, array $blocks = [])
    {
        // line 6
        echo "    <link rel=\"stylesheet\" href=\"/css/login.css\">
";
    }

    // line 10
    public function block_content($context, array $blocks = [])
    {
        // line 11
        echo "
    ";
        // line 12
        if ((isset($context["error"]) ? $context["error"] : null)) {
            // line 13
            echo "        <p>Invalid login credentials, try again or <a href=\"/register\">register</a></p>
    ";
        }
        // line 15
        echo "
    <form class=\"login100-form validate-form\" method=\"post\">
        <span class=\"login100-form-title p-b-55\">Login</span>
        <div class=\"wrap-input100 validate-input m-b-16\" >
            <input class=\"input100\" type=\"email\" name=\"email\" placeholder=\"Email\">\t\t
        </div>
        <br>
        <div class=\"wrap-input100 validate-input m-b-16\" >
            <input class=\"input100\" type=\"password\" name=\"password\" placeholder=\"Password\">
        </div>
        <br>
        <div class=\"contact100-form-checkbox m-l-4\">
            <input class=\"input-checkbox100\" id=\"ckb1\" type=\"checkbox\" name=\"remember-me\">
            <label class=\"label-checkbox100\" for=\"ckb1\">
                Remember me
            </label>
        </div>

        <div class=\"container-login100-form-btn p-t-25\">
            <button type=\"submit\" class=\"login100-form-btn\">Login</button>
          
        </div>
        <div class=\"text-center w-full p-t-42 p-b-22\">
            <span class=\"txt1\">Or login with</span>
        </div>

        <a href=\"#\" class=\"btn-face m-b-10\"><i class=\"fa fa-facebook-official\"></i>Facebook</a>
        <a href=\"#\" class=\"btn-google m-b-10\">Google</a>

        <div class=\"text-center w-full p-t-115\">
            <span class=\"txt1\">
                Not a member?
            </span>

            <a class=\"txt1 bo1 hov1\" href=\"/register\">Sign up now</a>
        </div>
    </form>

";
    }

    public function getTemplateName()
    {
        return "login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  67 => 15,  63 => 13,  61 => 12,  58 => 11,  55 => 10,  50 => 6,  47 => 5,  41 => 3,  31 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}login{% endblock %}

{% block headAdd %}
    <link rel=\"stylesheet\" href=\"/css/login.css\">
{% endblock %}


{% block content %}

    {% if error %}
        <p>Invalid login credentials, try again or <a href=\"/register\">register</a></p>
    {% endif %}

    <form class=\"login100-form validate-form\" method=\"post\">
        <span class=\"login100-form-title p-b-55\">Login</span>
        <div class=\"wrap-input100 validate-input m-b-16\" >
            <input class=\"input100\" type=\"email\" name=\"email\" placeholder=\"Email\">\t\t
        </div>
        <br>
        <div class=\"wrap-input100 validate-input m-b-16\" >
            <input class=\"input100\" type=\"password\" name=\"password\" placeholder=\"Password\">
        </div>
        <br>
        <div class=\"contact100-form-checkbox m-l-4\">
            <input class=\"input-checkbox100\" id=\"ckb1\" type=\"checkbox\" name=\"remember-me\">
            <label class=\"label-checkbox100\" for=\"ckb1\">
                Remember me
            </label>
        </div>

        <div class=\"container-login100-form-btn p-t-25\">
            <button type=\"submit\" class=\"login100-form-btn\">Login</button>
          
        </div>
        <div class=\"text-center w-full p-t-42 p-b-22\">
            <span class=\"txt1\">Or login with</span>
        </div>

        <a href=\"#\" class=\"btn-face m-b-10\"><i class=\"fa fa-facebook-official\"></i>Facebook</a>
        <a href=\"#\" class=\"btn-google m-b-10\">Google</a>

        <div class=\"text-center w-full p-t-115\">
            <span class=\"txt1\">
                Not a member?
            </span>

            <a class=\"txt1 bo1 hov1\" href=\"/register\">Sign up now</a>
        </div>
    </form>

{% endblock %}
", "login.html.twig", "C:\\xampp\\htdocs\\ipd17-php-project\\templates\\login.html.twig");
    }
}
