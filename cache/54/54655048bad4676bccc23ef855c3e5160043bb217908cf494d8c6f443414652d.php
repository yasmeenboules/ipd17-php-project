<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/categories_addedit.html.twig */
class __TwigTemplate_5292bec9cb6217c5faefabc8e1bc841bbe2d07456e9d54373c6e8d815b131dd6 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "admin/categories_addedit.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        echo "   

    ";
        // line 5
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 6
            echo "        <ul>
            ";
            // line 7
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errorList"]) ? $context["errorList"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 8
                echo "                <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 10
            echo "        </ul>
    ";
        }
        // line 11
        echo "  
    
    <h3>";
        // line 13
        if ($this->getAttribute((isset($context["v"]) ? $context["v"] : null), "id", [])) {
            echo "Update";
        } else {
            echo "Add";
        }
        echo " Category</h3>

    <div class=\"form\">
         <!-- <div id=\"sendmessage\">A category has been added successfully.</div> 
          <div id=\"errormessage\"></div>
          -->
          <form method=\"post\" role=\"form\">
            
            <div class=\"form-row\">
              <div class=\"form-group col-md-4\"></div>
              <div class=\"form-group col-md-1\">Name</div>
              <div class=\"form-group col-md-3\">        
                <input type=\"text\" class=\"form-control\" name=\"name\" id=\"name\" placeholder=\"Description\" value=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "name", []), "html", null, true);
        echo "\" />
                <div class=\"validation\"></div>
              </div>
                <div class=\"form-group col-md-4\"></div>  
            </div>
      
            <br><br>
            
           <div class=\"form-row\">
              <div class=\"form-group col-md-4\"></div>
            <div class=\"text-center \">
                <input type=\"submit\" value=\"";
        // line 36
        if ($this->getAttribute((isset($context["v"]) ? $context["v"] : null), "id", [])) {
            echo "Save";
        } else {
            echo "Add";
        }
        echo " Categpry\">
            </div>
            <div class=\"form-group col-md-4\">  
              </div>
           </div>
          </form>
             
    </div>
                
   ";
    }

    public function getTemplateName()
    {
        return "admin/categories_addedit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  104 => 36,  90 => 25,  71 => 13,  67 => 11,  63 => 10,  54 => 8,  50 => 7,  47 => 6,  45 => 5,  39 => 3,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block body %}   

    {% if errorList %}
        <ul>
            {% for error in errorList %}
                <li>{{error}}</li>
            {% endfor %}
        </ul>
    {% endif %}  
    
    <h3>{% if v.id %}Update{% else %}Add{% endif %} Category</h3>

    <div class=\"form\">
         <!-- <div id=\"sendmessage\">A category has been added successfully.</div> 
          <div id=\"errormessage\"></div>
          -->
          <form method=\"post\" role=\"form\">
            
            <div class=\"form-row\">
              <div class=\"form-group col-md-4\"></div>
              <div class=\"form-group col-md-1\">Name</div>
              <div class=\"form-group col-md-3\">        
                <input type=\"text\" class=\"form-control\" name=\"name\" id=\"name\" placeholder=\"Description\" value=\"{{v.name}}\" />
                <div class=\"validation\"></div>
              </div>
                <div class=\"form-group col-md-4\"></div>  
            </div>
      
            <br><br>
            
           <div class=\"form-row\">
              <div class=\"form-group col-md-4\"></div>
            <div class=\"text-center \">
                <input type=\"submit\" value=\"{% if v.id %}Save{% else %}Add{% endif %} Categpry\">
            </div>
            <div class=\"form-group col-md-4\">  
              </div>
           </div>
          </form>
             
    </div>
                
   {% endblock%}
", "admin/categories_addedit.html.twig", "C:\\xampp\\htdocs\\ipd17-php-project\\templates\\admin\\categories_addedit.html.twig");
    }
}
