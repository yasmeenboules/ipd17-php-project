<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* register.html.twig */
class __TwigTemplate_a39731b49938c686f3622c12bac7dbedbbf8fe3602ae12819417407c23e6c49a extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'headAdd' => [$this, 'block_headAdd'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "register.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        echo "registration ";
    }

    // line 5
    public function block_headAdd($context, array $blocks = [])
    {
        // line 6
        echo "    <link rel=\"stylesheet\" href=\"/css/register.css\">
     <script>
        // for user pleasant to tell him as he typing not after pressing submit
        \$(document).ready(function(){
            \$('input[name=email]').keyup(function(){
                var email = \$('input[name=email]').val();
                \$('#isTaken').load(\"/isemailregistered/\"+email);           
            });
        });
    </script>  

";
    }

    // line 19
    public function block_content($context, array $blocks = [])
    {
        echo "   
    ";
        // line 20
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 21
            echo "        <ul>
            ";
            // line 22
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errorList"]) ? $context["errorList"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 23
                echo "                <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 25
            echo "        </ul>
    ";
        }
        // line 27
        echo "
    <div data-spy=\"scroll\" data-target=\".navbar\" data-offset=\"60\">

        <div class=\"container\">
            <div class=\"page-header\">
                <h2>Sign Up</h2>
            </div>
            <form name=\"myForm\" onsubmit=\"return validateForm()\" class=\"form-horizontal\" role=\"form\"  method=\"post\">

                <div class=\"form-group\">
                    <label class=\"control-label col-sm-4\" for=\"firstname\"><span style=\"color:red;\">*</span> First Name:</label>
                    <div class=\"col-sm-6\">
                        <input type=\"firstname\" class=\"form-control\" id=\"firstname\" placeholder=\"Enter Your First Name\" name=\"firstname\">
                    </div>

                </div>

                <div class=\"form-group\">
                    <label class=\"control-label col-sm-4\" for=\"lastname\"><span style=\"color:red;\">*</span> Last Name:</label>
                    <div class=\"col-sm-6\">
                        <input type=\"lastname\" class=\"form-control\" id=\"lastname\" placeholder=\"Enter Your Last Name\" name=\"lastname\">
                    </div>

                </div>
                
                 <div class=\"form-group\">
                    <label class=\"control-label col-sm-4\" for=\"email\"><span style=\"color:red;\">*</span> Email:</label>
                    <div class=\"col-sm-6\">
                        <input type=\"email\" class=\"form-control\" id=\"email\" placeholder=\"Enter Your Email\" name=\"email\">
                    </div>
                    <span class=\"errorMessage\" id=\"isTaken\"></span><br>
                </div>

                <div class=\"form-group\">
                    <label class=\"control-label col-sm-4\" for=\"pwd1\"><span style=\"color:red;\">*</span> Password:</label>
                    <div class=\"col-sm-6\">
                        <input type=\"password\" class=\"form-control\" id=\"pwd1\" placeholder=\"No special characters, at least one letter, one capital letter, one number\" name=\"pwd1\">
                    </div>

                </div>

                <div class=\"form-group\">
                    <label class=\"control-label col-sm-4\" for=\"pwd2\"><span style=\"color:red;\">*</span> Password confirmation:</label>
                    <div class=\"col-sm-6\">
                        <input type=\"password\" class=\"form-control\" id=\"pwd2\" name=\"pwd2\">
                    </div>
                </div>

               

                <div class=\"form-group\">        
                    <div class=\"col-sm-offset-5 col-sm-4\">
                        <button type=\"submit\" class=\"btn btn-primary btn-block\" name=\"btn-signup\" id=\"signup\">Sign Up</button>
                    </div>
                </div>
            </form>
            <br /><br /><br />
        </div>
    </div>

";
    }

    public function getTemplateName()
    {
        return "register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 27,  88 => 25,  79 => 23,  75 => 22,  72 => 21,  70 => 20,  65 => 19,  50 => 6,  47 => 5,  41 => 3,  31 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}registration {% endblock %}

{% block headAdd %}
    <link rel=\"stylesheet\" href=\"/css/register.css\">
     <script>
        // for user pleasant to tell him as he typing not after pressing submit
        \$(document).ready(function(){
            \$('input[name=email]').keyup(function(){
                var email = \$('input[name=email]').val();
                \$('#isTaken').load(\"/isemailregistered/\"+email);           
            });
        });
    </script>  

{% endblock %}

{% block content %}   
    {% if errorList %}
        <ul>
            {% for error in errorList %}
                <li>{{error}}</li>
                {% endfor %}
        </ul>
    {% endif %}

    <div data-spy=\"scroll\" data-target=\".navbar\" data-offset=\"60\">

        <div class=\"container\">
            <div class=\"page-header\">
                <h2>Sign Up</h2>
            </div>
            <form name=\"myForm\" onsubmit=\"return validateForm()\" class=\"form-horizontal\" role=\"form\"  method=\"post\">

                <div class=\"form-group\">
                    <label class=\"control-label col-sm-4\" for=\"firstname\"><span style=\"color:red;\">*</span> First Name:</label>
                    <div class=\"col-sm-6\">
                        <input type=\"firstname\" class=\"form-control\" id=\"firstname\" placeholder=\"Enter Your First Name\" name=\"firstname\">
                    </div>

                </div>

                <div class=\"form-group\">
                    <label class=\"control-label col-sm-4\" for=\"lastname\"><span style=\"color:red;\">*</span> Last Name:</label>
                    <div class=\"col-sm-6\">
                        <input type=\"lastname\" class=\"form-control\" id=\"lastname\" placeholder=\"Enter Your Last Name\" name=\"lastname\">
                    </div>

                </div>
                
                 <div class=\"form-group\">
                    <label class=\"control-label col-sm-4\" for=\"email\"><span style=\"color:red;\">*</span> Email:</label>
                    <div class=\"col-sm-6\">
                        <input type=\"email\" class=\"form-control\" id=\"email\" placeholder=\"Enter Your Email\" name=\"email\">
                    </div>
                    <span class=\"errorMessage\" id=\"isTaken\"></span><br>
                </div>

                <div class=\"form-group\">
                    <label class=\"control-label col-sm-4\" for=\"pwd1\"><span style=\"color:red;\">*</span> Password:</label>
                    <div class=\"col-sm-6\">
                        <input type=\"password\" class=\"form-control\" id=\"pwd1\" placeholder=\"No special characters, at least one letter, one capital letter, one number\" name=\"pwd1\">
                    </div>

                </div>

                <div class=\"form-group\">
                    <label class=\"control-label col-sm-4\" for=\"pwd2\"><span style=\"color:red;\">*</span> Password confirmation:</label>
                    <div class=\"col-sm-6\">
                        <input type=\"password\" class=\"form-control\" id=\"pwd2\" name=\"pwd2\">
                    </div>
                </div>

               

                <div class=\"form-group\">        
                    <div class=\"col-sm-offset-5 col-sm-4\">
                        <button type=\"submit\" class=\"btn btn-primary btn-block\" name=\"btn-signup\" id=\"signup\">Sign Up</button>
                    </div>
                </div>
            </form>
            <br /><br /><br />
        </div>
    </div>

{% endblock %}



", "register.html.twig", "C:\\xampp\\htdocs\\ipd17-php-project\\templates\\register.html.twig");
    }
}
