<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* master.html.twig */
class __TwigTemplate_b25f657bebba3ce9a1e09940df558670128dce631c67ab7394b7edabd4b6aced extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'headAdd' => [$this, 'block_headAdd'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
    <head>
        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>
        <link rel=\"stylesheet\" href=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css\">
        <script src=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js\"></script>
        <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'>

        <!-- Bootstrap -->
        ";
        // line 13
        echo "
        <!-- Font Awesome -->
        <link rel=\"stylesheet\" href=\"/css/font-awesome.min.css\">

        <!-- Custom CSS -->
        <link rel=\"stylesheet\" href=\"/css/owl.carousel.css\">
        <link rel=\"stylesheet\" href=\"/css/style.css\">
        <link rel=\"stylesheet\" href=\"/css/responsive.css\">
        <title>";
        // line 21
        $this->displayBlock('title', $context, $blocks);
        echo "</title>

        ";
        // line 23
        $this->displayBlock('headAdd', $context, $blocks);
        // line 25
        echo "
    </head>
    <body>
        <div id=\"signUpPage\" class=\"header-area\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-md-8\">
                        <div class=\"user-menu\">
                            <ul>
                                <li><a href=\"/register\"><i class=\"fa fa-user\"></i>Sign up</a></li>
                                <li><a href=\"/login\"><i class=\"fa fa-user\"></i> Login</a></li>
                                <li><a href=\"#\"><i class=\"fa fa-heart\"></i> Wishlist</a></li>
                                <li><a href=\"cart.html\"><i class=\"fa fa-user\"></i> My Cart</a></li>
                                <li><a href=\"checkout.html\"><i class=\"fa fa-user\"></i> Checkout</a></li> 
                                ";
        // line 39
        if (((isset($context["user"]) ? $context["user"] : null) == "admin")) {
            // line 40
            echo "                                    <li><a href=\"checkout.html\"><i class=\"fa fa-user\"></i> Admin</a></li> 
                                ";
        }
        // line 42
        echo "                            </ul>
                        </div>
                    </div>

                    <div class=\"col-md-4\">
                        <div class=\"header-right\">
                            <ul class=\"list-unstyled list-inline\">
                                <li class=\"dropdown dropdown-small\">
                                    <a data-toggle=\"dropdown\" data-hover=\"dropdown\" class=\"dropdown-toggle\" href=\"#\"><span class=\"key\">language :</span><span class=\"value\">English </span><b class=\"caret\"></b></a>
                                    <ul class=\"dropdown-menu\">
                                        <li><a href=\"#\">English</a></li>
                                        <li><a href=\"#\">French</a></li>
                                    </ul>
                                </li>
                                <li><a href=\"/logout\"><i class=\"fa fa-user\"></i> Sign out</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- End header area -->

        <div class=\"site-branding-area\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-sm-6\">
                        <div class=\"logo\">
                            <h1><a href=\"/\"><img src=\"/img/logo.png\"></a></h1>
                        </div>
                    </div>

                    <div class=\"col-sm-6\">
                        <div class=\"shopping-item\">
                            <a href=\"cart.html\">Cart - <span class=\"cart-amunt\">\$100</span> <i class=\"fa fa-shopping-cart\"></i> <span class=\"product-count\">5</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- End site branding area -->

        <div class=\"mainmenu-area\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"navbar-header\">
                        <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                            <span class=\"sr-only\">Toggle navigation</span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                        </button>
                    </div> 
                    <div class=\"navbar-collapse collapse\">
                        <ul class=\"nav navbar-nav\">
                            <li><a href=\"./\">Home</a></li>
                            <li><a href=\"/shop\">Shop</a></li>
                            <li><a href=\"single-product.html\">Single item</a></li>
                            <li><a href=\"cart.html\">Cart</a></li>
                            <li><a href=\"checkout.html\">Checkout</a></li>
                            <li><a a href=\"#categories\">Category</a></li>

                        </ul>
                    </div>  
                </div>
            </div>
        </div>
        <div class=\"product-big-title-area\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-md-12\">
                        <div class=\"product-bit-title text-center\">
                            <h2>look.ipd17.com</h2>
                            <p>We specialize in your look!</p> 
                        </div>
                    </div>
                </div>
            </div>
        </div>

        ";
        // line 120
        $this->displayBlock('content', $context, $blocks);
        // line 122
        echo "
        <div class=\"footer-top-area\">
            <div class=\"zigzag-bottom\"></div>
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-md-3 col-sm-6\">
                        <div class=\"footer-about-us\">
                            <h2>L<span>ook</span></h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis sunt id doloribus vero quam laborum quas alias dolores blanditiis iusto consequatur, modi aliquid eveniet eligendi iure eaque ipsam iste, pariatur omnis sint! Suscipit, debitis. </p>
                            <a href=\"#\">Contact us</a>
                            <div class=\"footer-social\">
                                <a href=\"#\" target=\"_blank\"><i class=\"fa fa-facebook\"></i></a>
                                <a href=\"#\" target=\"_blank\"><i class=\"fa fa-twitter\"></i></a>
                                <a href=\"#\" target=\"_blank\"><i class=\"fa fa-youtube\"></i></a>
                                <a href=\"#\" target=\"_blank\"><i class=\"fa fa-linkedin\"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class=\"col-md-3 col-sm-6\">
                        <div class=\"footer-menu\">
                            <h2 class=\"footer-wid-title\">User Navigation </h2>
                            <ul>
                                <li><a href=\"#\">My account</a></li>
                                <li><a href=\"#\">Order history</a></li>
                                <li><a href=\"#\">Wishlist</a></li>
                                <li><a href=\"#\">Vendor contact</a></li>
                                <li><a href=\"#\">Front page</a></li>
                            </ul>                        
                        </div>
                    </div>

                    <div class=\"col-md-3 col-sm-6\">
                        <div class=\"footer-menu\">
                            <h2 id=\"categories\" class=\"footer-wid-title\">Categories</h2>
                            <ul>
                                <li><a href=\"#\">Clothing</a></li>
                                <li><a href=\"#\">Shoes</a></li>
                                <li><a href=\"#\">Beauty</a></li>
                                <li><a href=\"#\">Sport fan Clothing</a></li>
                                <li><a href=\"#\">Handbags & Accessories</a></li>
                            </ul>                        
                        </div>
                    </div>

                    <div class=\"col-md-3 col-sm-6\">
                        <div class=\"footer-newsletter\">
                            <h2 class=\"footer-wid-title\">Newsletter</h2>
                            <p>Sign up to our newsletter and get exclusive deals you wont find anywhere else straight to your inbox!</p>
                            <div class=\"newsletter-form\">
                                <form action=\"#\">
                                    <input type=\"email\" placeholder=\"Type your email\">
                                    <input type=\"submit\" value=\"Subscribe\">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- End footer top area -->

        <div class=\"footer-bottom-area\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-md-8\">
                        <div class=\"copyright\">
                            <p>&copy; 2019 LookFashion. All Rights Reserved. </p>
                        </div>
                    </div>

                    <div class=\"col-md-4\">
                        <div class=\"footer-card-icon\">
                            <i class=\"fa fa-cc-discover\"></i>
                            <i class=\"fa fa-cc-mastercard\"></i>
                            <i class=\"fa fa-cc-paypal\"></i>
                            <i class=\"fa fa-cc-visa\"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- End footer bottom area -->
        <footer class=\"container-fluid text-center\">
            <a href=\"#signUpPage\" title=\"To Top\">
                <span class=\"glyphicon glyphicon-chevron-up\"></span>
            </a>
            <p>look.ipd17.com</p>\t\t
        </footer>
        <!-- Latest jQuery form server -->
        <script src=\"https://code.jquery.com/jquery.min.js\"></script>

        <!-- Bootstrap JS form CDN -->
        <script src=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js\"></script>

        <!-- jQuery sticky menu -->
        <script src=\"/js/owl.carousel.min.js\"></script>
        <script src=\"/js/jquery.sticky.js\"></script>

        <!-- jQuery easing -->
        <script src=\"/js/jquery.easing.1.3.min.js\"></script>

        <!-- Main Script -->
        <script src=\"/js/main.js\"></script>

        <!-- Slider -->
        <script type=\"text/javascript\" src=\"/js/bxslider.min.js\"></script>
        <script type=\"text/javascript\" src=\"/js/script.slider.js\"></script>
    </body>
</html>";
    }

    // line 21
    public function block_title($context, array $blocks = [])
    {
    }

    // line 23
    public function block_headAdd($context, array $blocks = [])
    {
        // line 24
        echo "        ";
    }

    // line 120
    public function block_content($context, array $blocks = [])
    {
        // line 121
        echo "        ";
    }

    public function getTemplateName()
    {
        return "master.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  293 => 121,  290 => 120,  286 => 24,  283 => 23,  278 => 21,  167 => 122,  165 => 120,  85 => 42,  81 => 40,  79 => 39,  63 => 25,  61 => 23,  56 => 21,  46 => 13,  33 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"en\">
    <head>
        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>
        <link rel=\"stylesheet\" href=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css\">
        <script src=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js\"></script>
        <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'>

        <!-- Bootstrap -->
        {#<link rel=\"stylesheet\" href=\"css/bootstrap.min.css\">#}

        <!-- Font Awesome -->
        <link rel=\"stylesheet\" href=\"/css/font-awesome.min.css\">

        <!-- Custom CSS -->
        <link rel=\"stylesheet\" href=\"/css/owl.carousel.css\">
        <link rel=\"stylesheet\" href=\"/css/style.css\">
        <link rel=\"stylesheet\" href=\"/css/responsive.css\">
        <title>{% block title %}{% endblock %}</title>

        {% block headAdd %}
        {% endblock %}

    </head>
    <body>
        <div id=\"signUpPage\" class=\"header-area\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-md-8\">
                        <div class=\"user-menu\">
                            <ul>
                                <li><a href=\"/register\"><i class=\"fa fa-user\"></i>Sign up</a></li>
                                <li><a href=\"/login\"><i class=\"fa fa-user\"></i> Login</a></li>
                                <li><a href=\"#\"><i class=\"fa fa-heart\"></i> Wishlist</a></li>
                                <li><a href=\"cart.html\"><i class=\"fa fa-user\"></i> My Cart</a></li>
                                <li><a href=\"checkout.html\"><i class=\"fa fa-user\"></i> Checkout</a></li> 
                                {% if user == 'admin' %}
                                    <li><a href=\"checkout.html\"><i class=\"fa fa-user\"></i> Admin</a></li> 
                                {% endif %}
                            </ul>
                        </div>
                    </div>

                    <div class=\"col-md-4\">
                        <div class=\"header-right\">
                            <ul class=\"list-unstyled list-inline\">
                                <li class=\"dropdown dropdown-small\">
                                    <a data-toggle=\"dropdown\" data-hover=\"dropdown\" class=\"dropdown-toggle\" href=\"#\"><span class=\"key\">language :</span><span class=\"value\">English </span><b class=\"caret\"></b></a>
                                    <ul class=\"dropdown-menu\">
                                        <li><a href=\"#\">English</a></li>
                                        <li><a href=\"#\">French</a></li>
                                    </ul>
                                </li>
                                <li><a href=\"/logout\"><i class=\"fa fa-user\"></i> Sign out</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- End header area -->

        <div class=\"site-branding-area\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-sm-6\">
                        <div class=\"logo\">
                            <h1><a href=\"/\"><img src=\"/img/logo.png\"></a></h1>
                        </div>
                    </div>

                    <div class=\"col-sm-6\">
                        <div class=\"shopping-item\">
                            <a href=\"cart.html\">Cart - <span class=\"cart-amunt\">\$100</span> <i class=\"fa fa-shopping-cart\"></i> <span class=\"product-count\">5</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- End site branding area -->

        <div class=\"mainmenu-area\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"navbar-header\">
                        <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                            <span class=\"sr-only\">Toggle navigation</span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                        </button>
                    </div> 
                    <div class=\"navbar-collapse collapse\">
                        <ul class=\"nav navbar-nav\">
                            <li><a href=\"./\">Home</a></li>
                            <li><a href=\"/shop\">Shop</a></li>
                            <li><a href=\"single-product.html\">Single item</a></li>
                            <li><a href=\"cart.html\">Cart</a></li>
                            <li><a href=\"checkout.html\">Checkout</a></li>
                            <li><a a href=\"#categories\">Category</a></li>

                        </ul>
                    </div>  
                </div>
            </div>
        </div>
        <div class=\"product-big-title-area\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-md-12\">
                        <div class=\"product-bit-title text-center\">
                            <h2>look.ipd17.com</h2>
                            <p>We specialize in your look!</p> 
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {% block content %}
        {% endblock %}

        <div class=\"footer-top-area\">
            <div class=\"zigzag-bottom\"></div>
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-md-3 col-sm-6\">
                        <div class=\"footer-about-us\">
                            <h2>L<span>ook</span></h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis sunt id doloribus vero quam laborum quas alias dolores blanditiis iusto consequatur, modi aliquid eveniet eligendi iure eaque ipsam iste, pariatur omnis sint! Suscipit, debitis. </p>
                            <a href=\"#\">Contact us</a>
                            <div class=\"footer-social\">
                                <a href=\"#\" target=\"_blank\"><i class=\"fa fa-facebook\"></i></a>
                                <a href=\"#\" target=\"_blank\"><i class=\"fa fa-twitter\"></i></a>
                                <a href=\"#\" target=\"_blank\"><i class=\"fa fa-youtube\"></i></a>
                                <a href=\"#\" target=\"_blank\"><i class=\"fa fa-linkedin\"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class=\"col-md-3 col-sm-6\">
                        <div class=\"footer-menu\">
                            <h2 class=\"footer-wid-title\">User Navigation </h2>
                            <ul>
                                <li><a href=\"#\">My account</a></li>
                                <li><a href=\"#\">Order history</a></li>
                                <li><a href=\"#\">Wishlist</a></li>
                                <li><a href=\"#\">Vendor contact</a></li>
                                <li><a href=\"#\">Front page</a></li>
                            </ul>                        
                        </div>
                    </div>

                    <div class=\"col-md-3 col-sm-6\">
                        <div class=\"footer-menu\">
                            <h2 id=\"categories\" class=\"footer-wid-title\">Categories</h2>
                            <ul>
                                <li><a href=\"#\">Clothing</a></li>
                                <li><a href=\"#\">Shoes</a></li>
                                <li><a href=\"#\">Beauty</a></li>
                                <li><a href=\"#\">Sport fan Clothing</a></li>
                                <li><a href=\"#\">Handbags & Accessories</a></li>
                            </ul>                        
                        </div>
                    </div>

                    <div class=\"col-md-3 col-sm-6\">
                        <div class=\"footer-newsletter\">
                            <h2 class=\"footer-wid-title\">Newsletter</h2>
                            <p>Sign up to our newsletter and get exclusive deals you wont find anywhere else straight to your inbox!</p>
                            <div class=\"newsletter-form\">
                                <form action=\"#\">
                                    <input type=\"email\" placeholder=\"Type your email\">
                                    <input type=\"submit\" value=\"Subscribe\">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- End footer top area -->

        <div class=\"footer-bottom-area\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-md-8\">
                        <div class=\"copyright\">
                            <p>&copy; 2019 LookFashion. All Rights Reserved. </p>
                        </div>
                    </div>

                    <div class=\"col-md-4\">
                        <div class=\"footer-card-icon\">
                            <i class=\"fa fa-cc-discover\"></i>
                            <i class=\"fa fa-cc-mastercard\"></i>
                            <i class=\"fa fa-cc-paypal\"></i>
                            <i class=\"fa fa-cc-visa\"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- End footer bottom area -->
        <footer class=\"container-fluid text-center\">
            <a href=\"#signUpPage\" title=\"To Top\">
                <span class=\"glyphicon glyphicon-chevron-up\"></span>
            </a>
            <p>look.ipd17.com</p>\t\t
        </footer>
        <!-- Latest jQuery form server -->
        <script src=\"https://code.jquery.com/jquery.min.js\"></script>

        <!-- Bootstrap JS form CDN -->
        <script src=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js\"></script>

        <!-- jQuery sticky menu -->
        <script src=\"/js/owl.carousel.min.js\"></script>
        <script src=\"/js/jquery.sticky.js\"></script>

        <!-- jQuery easing -->
        <script src=\"/js/jquery.easing.1.3.min.js\"></script>

        <!-- Main Script -->
        <script src=\"/js/main.js\"></script>

        <!-- Slider -->
        <script type=\"text/javascript\" src=\"/js/bxslider.min.js\"></script>
        <script type=\"text/javascript\" src=\"/js/script.slider.js\"></script>
    </body>
</html>", "master.html.twig", "C:\\xampp\\htdocs\\ipd17-php-project\\templates\\master.html.twig");
    }
}
