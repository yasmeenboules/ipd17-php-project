<?php

////////////////////////////   categories    ////////////////////

$app->get('/admin/categories/list', function() use ($app) {
    if (!isset($_SESSION['user']) || $_SESSION['user']['role'] != 'admin') {
        $app->redirect('/forbidden');
        return;
    }
    $list = DB::query("SELECT * FROM categories");
    $app->render('admin/categories_list.html.twig', array('list' => $list));
});

// STATE 1: first show
$app->get('/admin/categories/:action(/:id)', function($action, $id = 0) use ($app) {
    if (!isset($_SESSION['user']) || $_SESSION['user']['role'] != 'admin') {
        $app->redirect('/forbidden');
        return;
    }
    if (($action == 'add' && $id != 0) || ($action == 'edit' && $id == 0)) {
        $app->notFound(); // 404 page
        return;
    }
    if ($action == 'add') {
        $app->render('admin/categories_addedit.html.twig');
    } else { // edit
        $category = DB::queryFirstRow("SELECT * FROM categories WHERE id=%i", $id);
        if (!$category) {
            $app->notFound();
            return;
        }
        $app->render('admin/categories_addedit.html.twig', array('v' => $category));
    }
})->conditions(array('action' => '(add|edit)'));

$app->post('/admin/categories/:action(/:id)', function($action, $id = 0) use ($app) {
    if (!isset($_SESSION['user']) || $_SESSION['user']['role'] != 'admin') {
        $app->redirect('/forbidden');
        return;
    }
    if (($action == 'add' && $id != 0) || ($action == 'edit' && $id == 0)) {
        $app->notFound(); // 404 page
        return;
    }
    //Are we receiving submission
    $name = $app->request()->post('name');
    
    //
    $errorList = array();
    // FIXME: sanitize html tags in name and name
   // validation for name
    if (strlen($name) < 2 || strlen($name) > 200) {
        array_push($errorList, "Description must be 2-200 characters long");
        $name = "";
    }
   
   // STATE 2: failed submission
    if ($errorList) { 
        $app->render('admin/categories_addedit.html.twig', array(
            'errorList' => $errorList,
            'v' => array('id' => $id,'name' => $name)));
    } else { // STATE 3: successful submission
        if ($action == 'add') {
            DB::insert('categories', array('name' => $name));
            $app->render('admin/categories_addedit_success.html.twig');
        } else {
            DB::update('categories', array('name' => $name), 'id=%i', $id);
            $app->render('admin/categories_addedit_success.html.twig', array('savedId' => $id));
        }
    }
})->conditions(array('action' => '(add|edit)'));

$app->get('/admin/categories/delete/:id', function($id) use ($app, $log) {
    if (!isset($_SESSION['user']) || $_SESSION['user']['role'] != 'admin') {
        $app->redirect('/forbidden');
        return;
    }//
    $category = DB::queryFirstRow("SELECT * FROM categories WHERE id=%i", $id);
    if (!$category) {
        $app->notFound();
        return;
    }
    $app->render('admin/categories_delete.html.twig', array('category' => $category));
});
$app->post('/admin/categories/delete/:id', function($id) use ($app, $log) {
    if (!isset($_SESSION['user']) || $_SESSION['user']['role'] != 'admin') {
        $app->redirect('/forbidden');
        return;
    }//
    if ($app->request()->post('confirmed') == 'true') {
        DB::delete("categories", "id=%i", $id);
        $app->render('admin/categories_delete_success.html.twig');
    } else {
        $app->redirect('/internalerror');
        return;
    }
});


