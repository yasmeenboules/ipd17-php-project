<?php

$app->get('/admin/items/list', function() use ($app) {
    $prodPerPage = 4;
    // calculate no. of items and to know how many pages we need to generate
    $prodCount = DB::queryFirstField("SELECT COUNT(*) FROM items");
    $totalPages = max(1,($prodCount + $prodPerPage - 1 ) / $prodPerPage);
    $app->render('admin/items_list.html.twig', array(
        'sessionUser' => @$_SESSION['user'],
        'totalPages' => $totalPages
            ));
});


$app->get('/ajax/admin/items/list/:page(/sortby/:order)', function($page, $order = 'id') use ($app, $log) {
    // give access only to admin
    if (!isset($_SESSION['user']) || $_SESSION['user']['role'] != 'admin') {
        $app->redirect('/forbidden');
        return;
    }
    $prodPerPage = 4;
    $prodCount = DB::queryFirstField("SELECT COUNT(*) FROM items");
//    if ($page * $prodPerPage > ($prodCount + $prodPerPage - 1)) { // TODO: make sure it's right
//        $app->notFound();
//        return;
//    }
    $skip = ($page - 1 ) * $prodPerPage;
    $itemsList = DB::query("SELECT I.id, I.name,I.description,I.price,I.imagePath,I.status, C.name  FROM items as I "
            . "INNER JOIN categories as C on I.categoryId=C.id ORDER BY %l LIMIT %l,%l", $order, $skip, $prodPerPage);
    $app->render('admin/ajax_items_list.html.twig', array(
        'list' => $itemsList));
    // print_r($itemsList);
    // $log->d('items: ' . print_r($itemsList, true));    
}); //->conditions(array('page' => '[1-9][0-9]*', 'order' => '(id|name|price)'));


