<?php
// view cart items
$app->get('/cart', function() use ($app,$log) {
     if (!$_SESSION['user']) {
        $log->debug('Attempt to see the cart contents for un unauthorized user from the IP: ' . $_SERVER['REMOTE_ADDR']);
        $app->redirect('/forbidden');
        return;
    }
    $cartitemList = DB::query(
                    "SELECT C.ID AS ID, I.id, quantity,"
                    . " name, description, imagePath,status, price "
                    . " FROM cartitems AS C "
                    . "INNER JOIN items AS I "
                    . " ON C.itemID = I.id "
                    . "WHERE sessionID=%s", session_id());
    $app->render('cart.html.twig', array(
        'cartitemList' => $cartitemList
    ));
});
// adding item right away to cart without showing details
$app->get('/cart/:itemID', function($itemID) use ($app,$log) {
     if (!$_SESSION['user']) {
        $log->debug('Attempt to see the cart contents for un unauthorized user from the IP: ' . $_SERVER['REMOTE_ADDR']);
        $app->redirect('/forbidden');
        return;
    }
    $quantity=1;
    $item = DB::queryFirstRow("SELECT * FROM cartitems WHERE itemID=%d AND sessionID=%s", $itemID, session_id());
    if ($item) {
        DB::update('cartitems', array(
            'sessionID' => session_id(),
            'itemID' => $itemID,
            'quantity' => $item['quantity'] + $quantity
                ), "itemID=%d AND sessionID=%s", $itemID, session_id());
    } else {
        DB::insert('cartitems', array(
            'sessionID' => session_id(),
            'itemID' => $itemID,
            'quantity' => $quantity
        ));
    }
    // show current contents of the cart
    $cartitemList = DB::query(
                    "SELECT C.ID as ID, I.id AS itemID, quantity,"
                    . " name, description, imagePath, price "
                    . " FROM cartitems as C "
                    . "INNER JOIN items AS I "
                    . " ON C.itemID = I.id "
                    . " WHERE sessionID=%s", session_id());
    $app->render('/cart.html.twig', array(
        'cartitemList' => $cartitemList
    ));
});

// add to cart from item/details page
$app->post('/cart', function() use ($app,$log) {
     if (!$_SESSION['user']) {
        $log->debug('Attempt to see the cart contents for un unauthorized user from the IP: ' . $_SERVER['REMOTE_ADDR']);
        $app->redirect('/forbidden');
        return;
    }
    $itemID = $app->request()->post('itemID');
    $quantity = $app->request()->post('quantity');
    // FIXME: make sure the item is not in the cart yet
    $item = DB::queryFirstRow("SELECT * FROM cartitems WHERE itemID=%d AND sessionID=%s", $itemID, session_id());
    if ($item) {
        DB::update('cartitems', array(
            'sessionID' => session_id(),
            'itemID' => $itemID,
            'quantity' => $item['quantity'] + $quantity
                ), "itemID=%d AND sessionID=%s", $itemID, session_id());
    } else {
        DB::insert('cartitems', array(
            'sessionID' => session_id(),
            'itemID' => $itemID,
            'quantity' => $quantity
        ));
    }
    // show current contents of the cart
    $cartitemList = DB::query(
                    "SELECT C.ID as ID, I.id AS itemID, quantity,"
                    . " name, description, imagePath, price "
                    . " FROM cartitems as C "
                    . "INNER JOIN items AS I "
                    . " ON C.itemID = I.id "
                    . " WHERE sessionID=%s", session_id());
    $app->render('/cart.html.twig', array(
        'cartitemList' => $cartitemList
    ));
});

// AJAX call, not used directy by user
// called by ajax cart twig
$app->get('/cart/update/:cartitemID/:quantity', function($cartitemID, $quantity) use ($app) {
    if ($quantity == 0) {
        DB::delete('cartitems', 'cartitems.ID=%d AND cartitems.sessionID=%s', $cartitemID, session_id());
    } else {
        DB::update('cartitems', array('quantity' => $quantity), 'cartitems.ID=%d AND cartitems.sessionID=%s', $cartitemID, session_id());
    }
    echo json_encode(DB::affectedRows() == 1);
});

// order handling
$app->map('/order', function () use ($app) {
    $totalBeforeTax = DB::queryFirstField(
                    "SELECT SUM(items.price * cartitems.quantity) "
                    . " FROM cartitems, items "
                    . " WHERE cartitems.sessionID=%s AND cartitems.itemID=items.id", session_id());
    // TODO: properly compute taxes, shipping, ...
    $shippingBeforeTax = 15.00;
    $taxes = ($totalBeforeTax + $shippingBeforeTax) * 0.15;
    $totalWithShippingAndTaxes = $totalBeforeTax + $shippingBeforeTax + $taxes;

    if ($app->request->isGet()) {
        $app->render('order.html.twig', array(
            'totalBeforeTax' => number_format($totalBeforeTax, 2),
            'shippingBeforeTax' => number_format($shippingBeforeTax, 2),
            'taxes' => number_format($taxes, 2),
            'totalWithShippingAndTaxes' => number_format($totalWithShippingAndTaxes, 2)
        ));
    } else {
        $name = $app->request->post('name');
        $email = $app->request->post('email');
        $address = $app->request->post('address');
        $postalCode = $app->request->post('postalCode');
        $phoneNumber = $app->request->post('phoneNumber');
        $valueList = array(
            'name' => $name,
            'email' => $email,
            'address' => $address,
            'postalCode' => $postalCode,
            'phoneNumber' => $phoneNumber
        );
        // FIXME: verify inputs - MUST DO IT IN A REAL SYSTEM
        $errorList = array();
        //
        if ($errorList) {
            $app->render('order.html.twig', array(
                'totalBeforeTax' => number_format($totalBeforeTax, 2),
                'shippingBeforeTax' => number_format($shippingBeforeTax, 2),
                'taxes' => number_format($taxes, 2),
                'totalWithShippingAndTaxes' => number_format($totalWithShippingAndTaxes, 2),
                'v' => $valueList
            ));
        } else { // SUCCESSFUL SUBMISSION
            DB::$error_handler = FALSE;
            DB::$throw_exception_on_error = TRUE;
            // PLACE THE ORDER
            try {
                DB::startTransaction();
                // 1. create summary record in 'orders' table (insert)
                DB::insert('orders', array(
                    'userID' => $_SESSION['user'] ? $_SESSION['user']['id'] : NULL,
                    'name' => $name,
                    'address' => $address,
                    'postalCode' => $postalCode,
                    'email' => $email,
                    'phoneNumber' => $phoneNumber,
                    'totalBeforeTax' => $totalBeforeTax,
                    'shippingBeforeTax' => $shippingBeforeTax,
                    'taxes' => $taxes,
                    'totalWithShippingAndTaxes' => $totalWithShippingAndTaxes,
                    'dateTimePlaced' => date('Y-m-d H:i:s')
                ));
                $orderID = DB::insertId();
                // 2. copy all records from cartitems to 'orderitems' (select & insert)
                $cartitemList = DB::query(
                                "SELECT itemID as origItemID, quantity, price"
                                . " FROM cartitems, items "
                                . " WHERE cartitems.itemID = items.id AND sessionID=%s", session_id());
                // add orderID to every sub-array (element) in $cartitemList
                array_walk($cartitemList, function(&$item, $key) use ($orderID) {
                    $item['orderID'] = $orderID;
                });
                /* This is the same as the following foreach loop:
                  foreach ($cartitemList as &$item) {
                  $item['orderID'] = $orderID;
                  } */
                DB::insert('orderitems', $cartitemList);
                // 3. delete cartitems for this user's session (delete)
                DB::delete('cartitems', "sessionID=%s", session_id());
                DB::commit();
                // TODO: send a confirmation email
                /*
                  $emailHtml = $app->view()->getEnvironment()->render('email_order.html.twig');
                  $headers = "MIME-Version: 1.0\r\n";
                  $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
                  mail($email, "Order " .$orderID . " placed ", $emailHtml, $headers);
                 */
                //
                $app->render('order_success.html.twig');
            } catch (MeekroDBException $e) {
                DB::rollback();
                database_error_handler(array(
                    'error' => $e->getMessage(),
                    'query' => $e->getQuery()
                ));
            }
        }
    }
})->via('GET', 'POST');

$app->get('/test', function() use ($app) {
    echo '<pre>\n';
    echo 'session_id is ' . session_id();
});


