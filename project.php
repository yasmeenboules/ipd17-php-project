<?php

session_cache_limiter(false);
session_start();


require_once 'vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));

// hang school pw gYRL0YiCtForZJC8
// hang home pw 4lJHbacvI8dgd9ij
// Yasmeen local home pw '7WVXZTRx8w8wAyQt'
// Yasmeen local school pw 'fLCz6TFk4MNhYfsK'
if ($_SERVER['SERVER_NAME'] == 'localhost') {//localhost
    DB::$user = 'lookdb';
    DB::$password = '4lJHbacvI8dgd9ij';
    DB::$dbName = 'lookdb';
    DB::$encoding = 'utf8';
    DB::$port = 3333;
} else {//ipd17.com
    DB::$user = 'cp4928_look';
    DB::$password = 'H0zaikX*Q1i&';
    DB::$dbName = 'cp4928_look';
    DB::$encoding = 'utf8';
}
// error handler for query syntax error
DB::$error_handler = 'database_error_handler';
// error handler for unable to connect or ... nonsql
DB::$nonsql_error_handler = 'database_error_handler';

function database_error_handler($params) {
    global $app, $log;
    $log->error("SQL Error:" . $params['error']);
    if (isset($params['query'])) {
        $log->error("SQL Query:" . $params['query']);
    }
    $app->render("internal_error.html.twig");
    http_response_code(500);
    die; // don't want to keep going if a query broke
}

// Slim creation and setup
$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig()
        ));

$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/cache'
);
$view->setTemplatesDirectory(dirname(__FILE__) . '/templates');

// add requriements to validate id giving
\Slim\Route::setDefaultConditions(array(
    'id' => '[0-9][0-9]*',
    'integer' => '(0|-?[1-9][0-9]*)'
));

if (!isset($_SESSION['user'])) {
    $_SESSION['user'] = array();
}
// set sessionUser as global for all twigs
$twig = $app->view()->getEnvironment();
$twig->addGlobal('sessionUser', $_SESSION['user']);
//get ip
function getUserIpAddr() {
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        //ip from share internet
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        //ip pass from proxy
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

require_once 'admin_categories.php';
require_once 'admin_items.php';
require_once 'admin_page.php';
require_once 'cart_order.php';

$app->get('/forbidden', function() use ($app) {
    $app->render('forbidden.html.twig', array('sessionUser' => @$_SESSION['user']));
});

$app->get('/ajax/isemailregistered/(:email)', function($email = "") use ($app) {
    $user = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
    if ($user) {
        echo "Email already registered";
    }
});

// STATE 1: first show
$app->get('/register', function() use ($app) {
    $app->render('register.html.twig');
});

$app->post('/register', function() use ($app, $log) {

    $firstname = $app->request()->post('firstname');
    $lastname = $app->request()->post('lastname');
    $email = $app->request()->post('email');
    $user = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
    $pwd1 = $app->request()->post('pwd1');
    $pwd2 = $app->request()->post('pwd2');
    //
    $errorList = array();

    if (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {
        array_push($errorList, "Email invalid");
        $email = "";
    } else if ($user) {
        array_push($errorList, "Email already taken");
    }

    if (strlen($firstname) < 2 || strlen($firstname) > 50) {
        array_push($errorList, "Name must be 2-50 characters long");
        $firstname = "";
    }

    if (strlen($lastname) < 2 || strlen($lastname) > 50) {
        array_push($errorList, "Name must be 2-50 characters long");
        $lastname = "";
    }

    if ($pwd1 != $pwd2) {
        array_push($errorList, "Passwords do not match");
    } else {
        if ((strlen($pwd1) < 6) || (preg_match("/[A-Z]/", $pwd1) == FALSE ) || (preg_match("/[a-z]/", $pwd1) == FALSE ) || (preg_match("/[0-9]/", $pwd1) == FALSE )) {
            array_push($errorList, "Password must be at least 6 characters long, "
                    . "with at least one uppercase, one lowercase, and one digit in it");
        }
    }

    if ($errorList) { // STATE 2: failed submission
        $app->render('register.html.twig', array(
            'errorList' => $errorList, 'v' => array('firstname' => $firstname, 'lastname' => $lastname, 'email' => $email)
        ));
    } else { // STATE 3: successful submission
        DB::insert('users', array('firstname' => $firstname, 'lastname' => $lastname, 'email' => $email, 'password' => $pwd1));
        $userId = DB::insertId();
        $log->debug("User registed with id=" . $userId);
        $app->render('register_success.html.twig');
    }
});

// STATE 1: first show
$app->get('/login', function() use ($app) {
    $app->render('login.html.twig');
});

$app->post('/login', function() use ($app, $log) {
    $email = $app->request()->post('email');
    $password = $app->request()->post('password');
    //
    $loginSuccessful = false;
    $user = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
    if ($user) {
        if ($user['password'] == $password) {
            $loginSuccessful = true;
        }
    }
    //
    if (!$loginSuccessful) { // array not empty -> errors present
        $log->info(sprintf("Login failed, email=%s, from IP=%s", $email, getUserIpAddr()));
        $app->render('login.html.twig', array('error' => true));
    } else { // STATE 3: successful submission
        unset($user['password']);
        $_SESSION['user'] = $user;
        
        $log->info(sprintf("Login successful, email=%s, from IP=%s", $email, getUserIpAddr()));
        $list = DB::query("SELECT * FROM items");
        $app->render('index.html.twig', array('list' => $list, 'sessionUser' => @$_SESSION['user']));
    }
});

$app->get('/logout', function() use ($app) {
    unset($_SESSION['user']);
    $list = DB::query("SELECT * FROM items");
    $app->render('index.html.twig', array('list' => $list, 'sessionUser' => @$_SESSION['user']));
});

$app->get('/shop', function() use ($app) {
    $prodPerPage = 3;
    $prodCount = DB::queryFirstField("SELECT COUNT(*) FROM items");
    $totalPages = max(1, ($prodCount + $prodPerPage - 1 ) / $prodPerPage);
    $app->render('shop.html.twig', array('sessionUser' => @$_SESSION['user'], 'totalPages' => $totalPages));
});

$app->get('/ajax/items/page/:page(/sortby/:order)', function($page, $order = 'id') use ($app, $log) {
    $prodPerPage = 3;
    $prodCount = DB::queryFirstField("SELECT COUNT(*) FROM items");
    if ($page * $prodPerPage > ($prodCount + $prodPerPage - 1)) { // TODO: make sure it's right
        $app->notFound();
        return;
    }
    $skip = ($page - 1 ) * $prodPerPage;
    $list = DB::query("SELECT * FROM items ORDER BY %l LIMIT %l,%l", $order, $skip, $prodPerPage);
    $app->render('ajax_items_page.html.twig', array('list' => $list));
    //print_r($itemsList);
    // $log->d('items: ' . print_r($itemsList, true));    
})->conditions(array('page' => '[1-9][0-9]*', 'order' => '(id|name|price)'));


// view item details with btn add to cart
$app->get('/item/details/:id', function($id = 0) use ($app,$log) {

    $list = DB::query("SELECT * FROM items");
    $item = DB::queryFirstRow("SELECT I.id, I.name,I.description,I.price,I.imagePath,I.status,I.brand, C.name as categoryName "
                    . "FROM items as I "
                    . "INNER JOIN categories as C "
                    . "on I.categoryId=C.id "
                    . "WHERE I.id=%i", $id);
    $app->render('item_details.html.twig', array('v' => $item, 'list' => $list, 'sessionUser' => @$_SESSION['user']));
});

$app->get('/ajax/search/:searchItem', function($searchItem = "") use ($app, $log) {
//    print_r($searchItem);
    $item = DB::query("SELECT * FROM items WHERE brand = %s", $searchItem);
    
    if (!$item) {
        $app->notFound();
        return;
    }
//    print_r($item);
    $app->render('search.html.twig', array('item' => $item, 'sessionUser' => @$_SESSION['user']));

});

$app->get('/', function() use ($app, $log) {
    $list = DB::query("SELECT * FROM items");
    $app->render('index.html.twig', array('list' => $list, 'sessionUser' => @$_SESSION['user']));
});



$app->get('/session', function() {
    echo '<pre>';
    print_r($_SESSION);
});

$app->run();
